# dgraph.nix

nix flake for dgraph

## About

This flake provides a [dgraph](https://dgraph.io) service configuration for [NixOS](https://nixos.org).

### About Dgraph

Dgraph is a no-SQL database with strong [GraphQL](https://graphql.org) support.

See [official website](https://dgraph.io) for details.

### About Nix

Nix is a package manager, language and ecosystem and the base of [NixOS](https://nixos.org) operating system.

See [official documentation](https://nixos.org/learn.html) for details.

### About Flakes

Flakes are a new experimental nix feature easing the way of packaging repositories for nix.

Further documentation:

- [Nix Manual Section](https://nixos.org/manual/nix/stable/command-ref/new-cli/nix3-flake.html)
- [NixOS Wiki Section](https://nixos.wiki/wiki/Flakes)

## Getting Started

### Run dgraph

```sh
# dgraph --help
nix run 'gitlab:dgrow/dgraph.nix' -- --help
```

### Get information about the outputs

```sh
nix show 'gitlab:dgrow/dgraph.nix'
```

### Start a container running dgraph

```sh
sudo nixos-container create dgraph --flake 'gitlab:dgrow/dgraph.nix#container'
sudo nixos-container start dgraph
```
